package utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;


public class ExplorerUtil{
	//入力文字列をSQL用文字列に変換
	public static String toDate(String str){
		if (!(StringUtils.isBlank(str))){
			try{
				if(existDate(str) == true){
					String date = str.replace('/', '-');
					if (date.matches("\\d{4}-\\d{2}-\\d{2}")){
						return date;
					}else{
						return "error";
					}
				}else{
					return "error";
				}
			}catch (Exception e){
				return "error";
			}
		}
		return null;
	}

	//日付検索
	public static int typeDateExp(String str1, String str2){
		if (StringUtils.isBlank(str1) && StringUtils.isBlank(str2)){
			return 1;	//両方入力無し→日付検索無し
		}else if (!(StringUtils.isBlank(str1)) && StringUtils.isBlank(str2)){
			return 2;	//開始日検索
		}else if (StringUtils.isBlank(str1) && !(StringUtils.isBlank(str2))){
			return 3;	//終了日検索
		}else if (!(StringUtils.isBlank(str1)) && !(StringUtils.isBlank(str2))){
			//そのまま期間指定
			try{
				if(compareDate(str1, str2) == true){
					return 4;
				}else{
					return 5;
				}
			} catch (ParseException e){
				return 6;
			}
		}
		return 0;
	}



	//入力された2つの日付を比較(前が古ければtrueを返す)
	public static boolean compareDate(String str1, String str2) throws ParseException{
		Date date1 = DateFormat.getDateInstance().parse(str1);
		Date date2 = DateFormat.getDateInstance().parse(str2);
		int diff = date1.compareTo(date2);
		if (diff <= 0){
			return true;
		}else{
			return false;
		}
	}

	//入力された日付が存在するものかどうか
	public static boolean existDate(String str) {
		try {
			DateFormat format = DateFormat.getDateInstance();
			format.setLenient(false);
			format.parse(str);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
