package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import beans.Dep;
import exception.SQLRuntimeException;

public class PulldownDao {

	public List<Branch> getAllBranch(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM sei_hanae.branch";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<Branch> branchList = toBranchList(rs);
			if (branchList.isEmpty() == true) {
				return null;
			} else {
				return branchList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<Dep> getAllDep(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM sei_hanae.dep";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<Dep> depList = toDepList(rs);
			if (depList.isEmpty() == true) {
				return null;
			} else {
				return depList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Branch> toBranchList(ResultSet rs)
			throws SQLException {

		List<Branch> ret = new ArrayList<Branch>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String branchName = rs.getString("name");

				Branch branch = new Branch();
				branch.setId(id);
				branch.setBranchName(branchName);

				ret.add(branch);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	private List<Dep> toDepList(ResultSet rs)
			throws SQLException {

		List<Dep> ret = new ArrayList<Dep>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String depName = rs.getString("name");

				Dep dep = new Dep();
				dep.setId(id);
				dep.setDepName(depName);

				ret.add(dep);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
