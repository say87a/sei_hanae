package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;


public class UserDao {

	public List<User> getUsers(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT sei_hanae.user.id, sei_hanae.user.login_id, sei_hanae.user.password, sei_hanae.user.name, user.branch_id, sei_hanae.branch.name, sei_hanae.user.dep_id, sei_hanae.dep.name, sei_hanae.user.status FROM sei_hanae.user, sei_hanae.branch, sei_hanae.dep WHERE sei_hanae.user.branch_id = sei_hanae.branch.id AND sei_hanae.user.dep_id = sei_hanae.dep.id";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<User> userList = showUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else {
				return userList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, String loginId,String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM sei_hanae.user WHERE user.login_id = ? AND user.password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM sei_hanae.user WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, String str) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM user WHERE account = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, str);


			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User registerCheck(Connection connection, String str, String key) {

		PreparedStatement ps = null;
		StringBuilder sql = new StringBuilder();
		try {
			sql.append("SELECT * FROM sei_hanae.user WHERE ");
			sql.append(key);
			sql.append("  = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, str);


			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branch = Integer.parseInt(rs.getString("branch_id"));
				int dep = Integer.parseInt(rs.getString("dep_id"));
				boolean status = rs.getBoolean("status");



				User user = new User();
				user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setName(name);
				user.setBranch(branch);
				user.setDep(dep);
				user.setStatus(status);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	private List<User> showUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("user.name");
				int branch = Integer.parseInt(rs.getString("branch_id"));
				String branchName = rs.getString("branch.name");
				int dep = Integer.parseInt(rs.getString("dep_id"));
				String depName = rs.getString("dep.name");
				boolean status = rs.getBoolean("status");



				User user = new User();
				user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setName(name);
				user.setBranch(branch);
				user.setBranchName(branchName);
				user.setDep(dep);
				user.setDepName(depName);
				user.setStatus(status);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO sei_hanae.user VALUES ( ");
			sql.append("null "); // id
			sql.append(", ?"); // login_id
			sql.append(", ?"); // password
			sql.append(", ?"); // name
			sql.append(", ?"); // branchl
			sql.append(", ?"); // dep
			sql.append(", true");	//status
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranch());
			ps.setInt(5, user.getDep());


			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE sei_hanae.user SET");
			sql.append("  id = ? ");
			sql.append(", login_id = ?");
			sql.append(", password = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", dep_id = ?");
			sql.append(", status = true");

			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getId());
			ps.setString(2, user.getLoginId());
			ps.setString(3, user.getPassword());
			ps.setString(4, user.getName());
			ps.setInt(5, user.getBranch());
			ps.setInt(6, user.getDep());
			ps.setInt(7, user.getId());


			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public void updateExceptPassword(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE sei_hanae.user SET");
			sql.append("  id = ? ");
			sql.append(", login_id = ?");
			sql.append(", password = password");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", dep_id = ?");
			sql.append(", status = true");

			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getId());
			ps.setString(2, user.getLoginId());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranch());
			ps.setInt(5, user.getDep());
			ps.setInt(6, user.getId());


			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public void stop(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE sei_hanae.user SET");
			sql.append("  id = ? ");
			sql.append(", login_id = ?");
			sql.append(", password = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", dep_id = ?");
			sql.append(", status = false");

			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getId());
			ps.setString(2, user.getLoginId());
			ps.setString(3, user.getPassword());
			ps.setString(4, user.getName());
			ps.setInt(5, user.getBranch());
			ps.setInt(6, user.getDep());
			ps.setInt(7, user.getId());


			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public void restart(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE sei_hanae.user SET");
			sql.append("  id = ? ");
			sql.append(", login_id = ?");
			sql.append(", password = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", dep_id = ?");
			sql.append(", status = true");

			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getId());
			ps.setString(2, user.getLoginId());
			ps.setString(3, user.getPassword());
			ps.setString(4, user.getName());
			ps.setInt(5, user.getBranch());
			ps.setInt(6, user.getDep());
			ps.setInt(7, user.getId());


			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

}
