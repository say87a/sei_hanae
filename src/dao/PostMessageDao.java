package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.PostMessage;
import exception.SQLRuntimeException;

public class PostMessageDao {

	public List<PostMessage> getPostMessages(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT sei_hanae.post.*, sei_hanae.branch.*, sei_hanae.dep.name, sei_hanae.user.name from sei_hanae.post, sei_hanae.user, sei_hanae.branch, sei_hanae.dep WHERE sei_hanae.post.user_id=sei_hanae.user.id AND sei_hanae.user.branch_id=sei_hanae.branch.id AND sei_hanae.user.dep_id=sei_hanae.dep.id");
			sql.append(" AND del = 0 AND status = true ORDER BY post_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<PostMessage> ret = showPostMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public PostMessage getPostMessages(Connection connection, int id, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM sei_hanae.post WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<PostMessage> postList = toPostMessageList(rs);
			if (postList.isEmpty() == true) {
				return null;
			} else if (2 <= postList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return postList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public static void update(Connection connection, PostMessage message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE sei_hanae.post SET ");
			sql.append("id = ?");
			sql.append(", title = ?");
			sql.append(", text = ?");
			sql.append(", category = ?");
			sql.append(", del = 1 ");
			sql.append(", user_id = ?");
			sql.append(", post_date = post_date");
			sql.append(" WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, message.getId());
			ps.setString(2, message.getTitle());
			ps.setString(3, message.getText());
			ps.setString(4, message.getCategory());
			ps.setInt(5, message.getUserId());
			ps.setInt(6, message.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<PostMessage> expPostMessages(Connection connection, String cate, String dt1, String dt2, int num) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT sei_hanae.post.*, sei_hanae.branch.*, sei_hanae.dep.name, sei_hanae.user.name from sei_hanae.post, sei_hanae.user, sei_hanae.branch, sei_hanae.dep WHERE sei_hanae.post.user_id=sei_hanae.user.id AND sei_hanae.user.branch_id=sei_hanae.branch.id AND sei_hanae.user.dep_id=sei_hanae.dep.id");
			sql.append(" AND CAST(post_date AS DATETIME) BETWEEN '");
			sql.append(dt1);
			sql.append(" 00:00:00' AND '");
			sql.append(dt2);
			sql.append(" 23:59:59'");
			sql.append(" AND category LIKE '%");
			sql.append(cate);
			sql.append("%'");
			sql.append(" AND del = 0 AND status = true ORDER BY post_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<PostMessage> ret = showPostMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public List<PostMessage> expPostMessagesStart(Connection connection, String cate, String dt1, int num) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT sei_hanae.post.*, sei_hanae.branch.*, sei_hanae.dep.name, sei_hanae.user.name from sei_hanae.post, sei_hanae.user, sei_hanae.branch, sei_hanae.dep WHERE sei_hanae.post.user_id=sei_hanae.user.id AND sei_hanae.user.branch_id=sei_hanae.branch.id AND sei_hanae.user.dep_id=sei_hanae.dep.id");
			sql.append(" AND CAST(post_date AS DATETIME) >= '");
			sql.append(dt1);
			sql.append(" 00:00:00' ");
			sql.append(" AND category LIKE '%");
			sql.append(cate);
			sql.append("%'");
			sql.append(" AND del = 0 AND status = true ORDER BY post_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<PostMessage> ret = showPostMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

		public List<PostMessage> expPostMessagesEnd(Connection connection, String cate, String dt2, int num) {
			PreparedStatement ps = null;
			try {
				StringBuilder sql = new StringBuilder();
				sql.append("SELECT sei_hanae.post.*, sei_hanae.branch.*, sei_hanae.dep.name, sei_hanae.user.name from sei_hanae.post, sei_hanae.user, sei_hanae.branch, sei_hanae.dep WHERE sei_hanae.post.user_id=sei_hanae.user.id AND sei_hanae.user.branch_id=sei_hanae.branch.id AND sei_hanae.user.dep_id=sei_hanae.dep.id");
				sql.append(" AND CAST(post_date AS DATETIME) <= '");
				sql.append(dt2);
				sql.append(" 00:00:00' ");
				sql.append(" AND category LIKE '%");
				sql.append(cate);
				sql.append("%'");
				sql.append(" AND del = 0 AND status = true ORDER BY post_date DESC limit " + num);

				ps = connection.prepareStatement(sql.toString());

				ResultSet rs = ps.executeQuery();
				List<PostMessage> ret = showPostMessageList(rs);
				return ret;
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}

	}

	public List<PostMessage> expPostMessages(Connection connection, String dt1, String dt2, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT sei_hanae.post.*, sei_hanae.branch.*, sei_hanae.dep.name, sei_hanae.user.name from sei_hanae.post, sei_hanae.user, sei_hanae.branch, sei_hanae.dep WHERE sei_hanae.post.user_id=sei_hanae.user.id AND sei_hanae.user.branch_id=sei_hanae.branch.id AND sei_hanae.user.dep_id=sei_hanae.dep.id");
			sql.append(" AND CAST(post_date AS DATETIME) BETWEEN '");
			sql.append(dt1);
			sql.append(" 00:00:00' AND '");
			sql.append(dt2);
			sql.append(" 23:59:59'");
			sql.append(" AND del = 0 AND status = true ORDER BY post_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<PostMessage> ret = showPostMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<PostMessage> expPostMessagesStart(Connection connection, String dt1, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT sei_hanae.post.*, sei_hanae.branch.*, sei_hanae.dep.name, sei_hanae.user.name from sei_hanae.post, sei_hanae.user, sei_hanae.branch, sei_hanae.dep WHERE sei_hanae.post.user_id=sei_hanae.user.id AND sei_hanae.user.branch_id=sei_hanae.branch.id AND sei_hanae.user.dep_id=sei_hanae.dep.id");
			sql.append(" AND CAST(post_date AS DATETIME) >= '");
			sql.append(dt1);
			sql.append(" 00:00:00' ");
			sql.append(" AND del = 0 AND status = true ORDER BY post_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<PostMessage> ret = showPostMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<PostMessage> expPostMessagesEnd(Connection connection, String dt2, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT sei_hanae.post.*, sei_hanae.branch.*, sei_hanae.dep.name, sei_hanae.user.name from sei_hanae.post, sei_hanae.user, sei_hanae.branch, sei_hanae.dep WHERE sei_hanae.post.user_id=sei_hanae.user.id AND sei_hanae.user.branch_id=sei_hanae.branch.id AND sei_hanae.user.dep_id=sei_hanae.dep.id");
			sql.append(" AND CAST(post_date AS DATETIME) <= '");
			sql.append(dt2);
			sql.append(" 23:59:59' ");
			sql.append(" AND del = 0 AND status = true ORDER BY post_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<PostMessage> ret = showPostMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//仮カテゴリ検索
	public List<PostMessage> expPostMessages(Connection connection, String cate, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT sei_hanae.post.*, sei_hanae.branch.*, sei_hanae.dep.name, sei_hanae.user.name from sei_hanae.post, sei_hanae.user, sei_hanae.branch, sei_hanae.dep WHERE sei_hanae.post.user_id=sei_hanae.user.id AND sei_hanae.user.branch_id=sei_hanae.branch.id AND sei_hanae.user.dep_id=sei_hanae.dep.id");
			sql.append(" AND category LIKE '%");
			sql.append(cate);
			sql.append("%'");
			sql.append(" AND del = 0 AND status = true ORDER BY post_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<PostMessage> ret = showPostMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<PostMessage> toPostMessageList(ResultSet rs)
			throws SQLException {

		List<PostMessage> ret = new ArrayList<PostMessage>();
		try {
			while (rs.next()) {
				String title = rs.getString("title");
				String text = rs.getString("text");
				String category = rs.getString("category");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				Timestamp insertDate = rs.getTimestamp("post_date");


				PostMessage message = new PostMessage();
				message.setTitle(title);
				message.setText(text);
				message.setCategory(category);
				message.setId(id);
				message.setUserId(userId);
				message.setInsertDate(insertDate);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

		private List<PostMessage> showPostMessageList(ResultSet rs)
				throws SQLException {

			List<PostMessage> ret = new ArrayList<PostMessage>();
			try {
				while (rs.next()) {
					String title = rs.getString("title");
					String text = rs.getString("text");
					String category = rs.getString("category");
					int id = rs.getInt("id");
					int userId = rs.getInt("user_id");
					int branchId = rs.getInt("branch.id");
					String branchName = rs.getString("branch.name");
					String depName = rs.getString("dep.name");
					String userName = rs.getString("user.name");
					Timestamp insertDate = rs.getTimestamp("post_date");


					PostMessage message = new PostMessage();
					message.setTitle(title);
					message.setText(text);
					message.setCategory(category);
					message.setId(id);
					message.setUserId(userId);
					message.setBranchId(branchId);
					message.setBranchName(branchName);
					message.setDepName(depName);
					message.setUserName(userName);
					message.setInsertDate(insertDate);

					ret.add(message);
				}
				return ret;
			} finally {
				close(rs);
			}
	}

		//投稿最大値の取得
		public PostMessage getMaxPostId (Connection connection){
			PreparedStatement ps = null;

			try{
				String sql = "SELECT * FROM sei_hanae.post WHERE sei_hanae.post.id = (SELECT MAX(sei_hanae.post.id) FROM sei_hanae.post)";
				ps = connection.prepareStatement(sql.toString());

				ResultSet rs = ps.executeQuery();
				List<PostMessage> postMessageList = toPostMessageList(rs);
				if (postMessageList.isEmpty() == true) {
					return null;
				} else if (2 <= postMessageList.size()) {
					throw new IllegalStateException("2 <= userList.size()");
				} else {
					return postMessageList.get(0);
				}
			} catch (SQLException e){
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}
		}

}
