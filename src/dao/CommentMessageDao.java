package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentMessageDao {

	public List<Comment> getCommentMessages(Connection connection, int lim) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT sei_hanae.comment.*, sei_hanae.branch.*, sei_hanae.dep.name, sei_hanae.user.name from sei_hanae.comment, sei_hanae.user, sei_hanae.branch, sei_hanae.dep WHERE sei_hanae.comment.user_id=sei_hanae.user.id AND sei_hanae.user.branch_id=sei_hanae.branch.id AND sei_hanae.user.dep_id=sei_hanae.dep.id ");
			sql.append(" AND del = 0 ORDER BY post_date ASC limit " + lim);

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<Comment> ret = showCommentMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public Comment getComment(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM sei_hanae.comment WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<Comment> commentList = toCommentMessageList(rs);
			if (commentList.isEmpty() == true) {
				return null;
			} else if (2 <= commentList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return commentList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public static void update(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE sei_hanae.comment SET ");
			sql.append("id = ?");
			sql.append(", text = ?");
			sql.append(", post_id = post_id");
			sql.append(", del = 1 ");
			sql.append(", user_id = ?");
			sql.append(", post_date = post_date");
			sql.append(" WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getId());
			ps.setString(2, comment.getText());
			ps.setInt(3, comment.getUserId());
			ps.setInt(4, comment.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Comment> toCommentMessageList(ResultSet rs)
			throws SQLException {

		List<Comment> ret = new ArrayList<Comment>();
		try {
			while (rs.next()) {
				String text = rs.getString("text");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				int postId = rs.getInt("post_id");
				Timestamp postDate = rs.getTimestamp("post_date");


				Comment comment = new Comment();
				comment.setText(text);
				comment.setPostId(postId);
				comment.setId(id);
				comment.setUserId(userId);
				comment.setPostDate(postDate);

				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	private List<Comment> showCommentMessageList(ResultSet rs)
			throws SQLException {

		List<Comment> ret = new ArrayList<Comment>();
		try {
			while (rs.next()) {
				String text = rs.getString("text");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				int branchId = rs.getInt("branch.id");
				String branchName = rs.getString("branch.name");
				String depName = rs.getString("dep.name");
				String userName = rs.getString("user.name");
				int postId = rs.getInt("post_id");
				Timestamp postDate = rs.getTimestamp("post_date");


				Comment comment = new Comment();
				comment.setText(text);
				comment.setPostId(postId);
				comment.setId(id);
				comment.setUserId(userId);
				comment.setBranchId(branchId);
				comment.setBranchName(branchName);
				comment.setDepName(depName);
				comment.setUserName(userName);
				comment.setPostDate(postDate);

				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
