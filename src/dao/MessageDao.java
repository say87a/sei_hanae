package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.PostMessage;
import exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, PostMessage message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO sei_hanae.post ( ");
			sql.append("id");
			sql.append(", title");
			sql.append(", text");
			sql.append(", category");
			sql.append(", post_date");
			sql.append(", user_id");
			sql.append(", del");
			sql.append(") VALUES (");
			sql.append("null"); // id
			sql.append(", ?"); // title
			sql.append(", ?"); // category
			sql.append(", ?"); // text
			sql.append(", CURRENT_TIMESTAMP"); // post_date
			sql.append(", ?"); // user_id
			sql.append(", 0");	//delフラグ
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, message.getTitle());
			ps.setString(2, message.getText());
			ps.setString(3, message.getCategory());
			ps.setInt(4, message.getUserId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


}
