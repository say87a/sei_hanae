package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Log;
import exception.SQLRuntimeException;

public class PostlogDao {

	//投稿の履歴
	public void insertPost(Connection connection, Log log) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO sei_hanae.postlog ( ");
			sql.append("id");
			sql.append(", user_id");
			sql.append(", post_id");
			sql.append(", style_type");
			sql.append(", update_date");
			sql.append(") VALUES (");
			sql.append("null"); // id
			sql.append(", ?"); // user_id
			sql.append(", ?"); // post_id
			sql.append(", 0"); // style_type
			sql.append(", CURRENT_TIMESTAMP"); // post_id
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, log.getUserId());
			ps.setInt(2, log.getPostId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//コメントの履歴
	public void insertComment(Connection connection, Log log) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO sei_hanae.postlog ( ");
			sql.append("id");
			sql.append(", user_id");
			sql.append(", post_id");
			sql.append(", style_type");
			sql.append(", update_date");
			sql.append(") VALUES (");
			sql.append("null"); // id
			sql.append(", ?"); // user_id
			sql.append(", ?"); // post_id
			sql.append(", 1"); // style_type
			sql.append(", CURRENT_TIMESTAMP"); // post_id
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, log.getUserId());
			ps.setInt(2, log.getPostId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<Log> getLog(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT sei_hanae.postlog.id, sei_hanae.user.name, sei_hanae.post.id, sei_hanae.post.title, sei_hanae.postlog.style_type, sei_hanae.postlog.update_date from sei_hanae.post, sei_hanae.user, sei_hanae.postlog WHERE sei_hanae.postlog.user_id=sei_hanae.user.id AND sei_hanae.postlog.post_id=sei_hanae.post.id  ORDER BY update_date DESC");
			sql.append(" limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Log> ret = toLogList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	private List<Log> toLogList(ResultSet rs)
			throws SQLException {

		List<Log> ret = new ArrayList<Log>();
		try {
			while (rs.next()) {
				String title = rs.getString("title");
				String name = rs.getString("name");
				int id = rs.getInt("postlog.id");
				int postId = rs.getInt("post.id");
				Timestamp updateDate = rs.getTimestamp("update_date");
				int styleType = rs.getInt("style_type");


				Log log = new Log();
				log.setTitle(title);
				log.setName(name);
				log.setId(id);
				log.setPostId(postId);
				log.setUpdateDate(updateDate);
				log.setStyleType(styleType);

				ret.add(log);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
