package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO sei_hanae.comment ( ");
			sql.append("id");
			sql.append(", text");
			sql.append(", post_date");
			sql.append(", user_id");
			sql.append(", post_id");
			sql.append(", del");
			sql.append(") VALUES (");
			sql.append("null"); // id
			sql.append(", ?"); // text
			sql.append(", CURRENT_TIMESTAMP"); // post_date
			sql.append(", ?"); // user_id
			sql.append(", ?"); // post_id
			sql.append(", 0"); // del
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, comment.getText());
			ps.setInt(2, comment.getUserId());
			ps.setInt(3, comment.getPostId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
