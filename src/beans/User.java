package beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String loginId;
	private String name;
	private String email;
	private String password;
	private int	branch ;
	private String branchName;
	private int dep;
	private String depName;
	private Date insertDate;
	private Date updateDate;
	private boolean status;

	public int getId() {
		return id;
	}

	public void setId (int id){
		this.id = id;
	}


	public String getLoginId() {
		return loginId;
	}

	public void setLoginId (String loginId){
		this.loginId = loginId;
	}



	public String getName() {
		return name;
	}

	public void setName (String name){
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail (String email){
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword (String password){
		this.password = password;
	}

	public int getBranch() {
		return branch;
	}

	public void setBranch (int branch){
		this.branch = branch;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName (String branchName){
		this.branchName = branchName;
	}

	public int getDep() {
		return dep;
	}

	public void setDep (int dep){
		this.dep = dep;
	}

	public String getDepName() {
		return depName;
	}

	public void setDepName (String depName){
		this.depName = depName;
	}

	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate (Date insertDate){
		this.insertDate = insertDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate (Date updateDate){
		this.updateDate = updateDate;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus (boolean status){
		this.status = status;
	}
}
