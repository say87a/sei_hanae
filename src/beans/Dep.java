package beans;

import java.io.Serializable;

public class Dep implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String depName;

	public int getId() {
		return id;
	}

	public void setId (int id){
		this.id = id;
	}

	public String getDepName() {
		return depName;
	}

	public void setDepName (String depName){
		this.depName = depName;
	}
}
