package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;


@WebServlet(urlPatterns = {"/HRadminuser"})
public class HRAdminUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ユーザー一覧を取得
		List<User> users = new UserService().getUsers();	//取り出したList<User>
		request.setAttribute("users", users);
		request.getRequestDispatcher("HRadminuser.jsp").forward(request, response);

		//TODO 停止ボタン制御、自ユーザは停止させない
	}


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
}