package controller;

/*import static chapter6.utils.CloseableUtil.*;*/


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Comment;
import beans.PostMessage;
import exception.NoRowsUpdatedRuntimeException;
import service.CommentService;
import service.MessageService;


@WebServlet(urlPatterns = { "/delete" })
@MultipartConfig(maxFileSize = 100000)
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		//getParameterで得た投稿IDから投稿情報を呼び出し
		String pdelId = request.getParameter("pdelId");
		if(pdelId != null){
			PostMessage deletePost = new MessageService().getMessage(Integer.parseInt(pdelId));
			try {
				new MessageService().update(deletePost);
			} catch (NoRowsUpdatedRuntimeException e) {
				session.removeAttribute("pdelId");
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				/*response.sendRedirect("./");
				return;*/
			}
		}else{
			messages.add("不正なアクセスです");
		}

		//getParameterで得たコメントIDから投稿情報を呼び出し
		String cdelId = request.getParameter("cdelId");
		if(cdelId != null){
			Comment deleteComment = new CommentService().getComment(Integer.parseInt(cdelId));
			try {
				new CommentService().update(deleteComment);
			} catch (NoRowsUpdatedRuntimeException e) {
				session.removeAttribute("cdelId");
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				/*response.sendRedirect("./");
				return;*/
			}
		}




		response.sendRedirect("./");
	}
}
