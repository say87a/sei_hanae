package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import beans.Comment;
import beans.Log;
import beans.User;
import service.CallLogService;
import service.CommentService;

@WebServlet(urlPatterns = {"/newcomment"})
public class NewComment extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getParameter("postId");
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");
			String commentLb = (request.getParameter("comment"));
			Comment comment = new Comment();
			comment.setPostId(Integer.parseInt(request.getParameter("postId")));
			comment.setText(commentLb);
			comment.setUserId(user.getId());

			Log  log = new Log();
			log.setPostId(Integer.parseInt(request.getParameter("postId")));
			log.setUserId(user.getId());
			new CallLogService().insertComment(log);

			new CommentService().register(comment);
			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);

			response.sendRedirect("./");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String comment = request.getParameter("comment");

		if (StringUtils.isBlank(comment) == true) {
			messages.add("コメントを入力してください");
		}

		if (500 < comment.length()) {
			messages.add("コメントは500文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
