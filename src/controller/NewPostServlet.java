package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import beans.Log;
import beans.PostMessage;
import beans.User;
import service.CallLogService;
import service.MessageService;

@WebServlet(urlPatterns = {"/newpost"})
public class NewPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("newpost.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			PostMessage message = new PostMessage();
			message.setTitle(request.getParameter("title"));
			message.setCategory(request.getParameter("category"));
			message.setText(request.getParameter("text"));
			message.setUserId(user.getId());
			new MessageService().register(message);

			Log log = new Log();
			log.setUserId(user.getId());
			int maxPostId = (new MessageService().getMaxPostId()).getId();
			log.setPostId(maxPostId);
			new CallLogService().insertPost(log);



			response.sendRedirect("./");
		} else {
			PostMessage message = new PostMessage();
			message.setTitle(request.getParameter("title"));
			message.setCategory(request.getParameter("category"));
			message.setText(request.getParameter("text"));
			session.setAttribute("input", message);
			session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("newpost.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String text = request.getParameter("text");
		String title = request.getParameter("title");
		String category = request.getParameter("category");

		if (StringUtils.isBlank(title) == true) {
			messages.add("タイトルを入力してください");
		}
		if (50 < title.length()) {
			messages.add("タイトルは50文字以下で入力してください");
		}
		if (StringUtils.isBlank(category) == true) {
			messages.add("カテゴリを入力してください");
		}
		if (10 < category.length()) {
			messages.add("カテゴリ名は10文字以下で入力してください");
		}
		if (StringUtils.isBlank(text) == true) {
			messages.add("本文を入力してください");
		}
		if (1000 < text.length()) {
			messages.add("本文は1000文字以下で入力してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
