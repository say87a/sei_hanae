package controller;

/*import static chapter6.utils.CloseableUtil.*;*/


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.UserService;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;


@WebServlet(urlPatterns = { "/HRstopUser" })
@MultipartConfig(maxFileSize = 100000)
public class HRStopUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
		ArrayList<String> messages = new ArrayList<>();
		HttpSession session = request.getSession();
		if(request.getParameter("id") != null){
			String changeId = request.getParameter("id");
			if (session.getAttribute("editUser") == null) {
				User editUser = new UserService().getUser(Integer.parseInt(changeId));
				session.setAttribute("editUser", editUser);
				request.getRequestDispatcher("HRadminuser").forward(request, response);
				return;
			}
		}else{
			messages.add("不正なアクセスです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("HRadminuser");
			return;
		}


	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();

		String changeId = request.getParameter("id");
		User editUser = new UserService().getUser(Integer.parseInt(changeId));
		session.setAttribute("editUser", editUser);

		if (editUser != null) {

			try {
				new UserService().stop(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				session.removeAttribute("editUser");
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("HRadminuser");
				return;
			}

			session.setAttribute("editUser", editUser);
			session.removeAttribute("editUser");
			response.sendRedirect("HRadminuser");
			return;
		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("HRadminuser");
			return;
		}
	}

}
