package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import service.CallListService;
import service.UserService;
import beans.Branch;
import beans.Dep;
import beans.User;

@WebServlet(urlPatterns = { "/HRsignup" })
public class HRSignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
		HttpServletResponse response) throws IOException, ServletException {

			User inputUser = (User) request.getAttribute ("inputUser");
			List <Branch> branchList = new CallListService().getAllBranch();
			request.setAttribute("branchList", branchList);
			List <Dep> depList = new CallListService().getAllDep();
			request.setAttribute("depList", depList);


			if (request.getAttribute("inputUser") != null){
				User signupUser = new UserService().getUser(inputUser.getId());
				request.setAttribute("signupUser", signupUser);
			}
			request.getRequestDispatcher("/HRsignup.jsp").forward(request, response);


	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();

		User signupUser = getSignupUser(request);
		request.setAttribute ("signupUser", signupUser);

		if (isValid(request, messages, signupUser) == true) {

			User user = new User();
			user.setLoginId(request.getParameter("loginId"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranch(Integer.parseInt(request.getParameter("branch")));
			user.setDep(Integer.parseInt(request.getParameter("dep")));
			/**
			 * TODO 登録から
			 */
			new UserService().register(user);

			//HttpSession session = request.getSession();
			//User signupUser = (User) session.getAttribute("signupUser");

			//if(signupUser != null){
				session.setAttribute("loginId",request.getParameter("loginId"));
				session.setAttribute("password",request.getParameter("password"));
				session.setAttribute("name",request.getParameter("name"));
				//signupUser.setPassword(request.getParameter("password"));
				//signupUser.setName(request.getParameter("name"));
				try{
					session.setAttribute("branch",request.getParameter("branch"));
					session.setAttribute("dep",request.getParameter("dep"));
					//signupUser.setBranch(Integer.parseInt(request.getParameter("branch")));
					//signupUser.setDep(Integer.parseInt(request.getParameter("dep")));
				}catch (NumberFormatException e){
					session.setAttribute("branch",0);
					session.setAttribute("dep",0);
					//signupUser.setBranch(0);
					//signupUser.setDep(0);
				}

			//session.setAttribute("signupUser", signupUser);
			session.removeAttribute("signupUser");
			response.sendRedirect("HRadminuser");
			return;
		} else {
			User user = new User();
			user.setLoginId(request.getParameter("loginId"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			try{
				user.setBranch(Integer.parseInt(request.getParameter("branch")));
				user.setDep(Integer.parseInt(request.getParameter("dep")));
			}catch (NumberFormatException e){
				user.setBranch(0);
				user.setDep(0);
			}
			session.setAttribute("signupUser", user);

			session.setAttribute("errorMessages", messages);
			response.sendRedirect("HRsignup");
			return;
		}
	}

	private User getSignupUser(HttpServletRequest request)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		User signupUser = (User) session.getAttribute("signupUser");

		if(signupUser != null){
			signupUser.setLoginId(request.getParameter("loginId"));
			signupUser.setPassword(request.getParameter("password"));
			signupUser.setName(request.getParameter("name"));
			try{
				signupUser.setBranch(Integer.parseInt(request.getParameter("branch")));
				signupUser.setDep(Integer.parseInt(request.getParameter("dep")));
			}catch (NumberFormatException e){
				signupUser.setBranch(0);
				signupUser.setDep(0);
			}
		}
		return signupUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages, User user){
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String repassword = request.getParameter("repassword");
		boolean passmatch = password.matches(repassword);
		String name = request.getParameter("name");

		if (StringUtils.isBlank(loginId) == true || loginId.length() < 6 || loginId.length() > 20){
			messages.add("ログインIDは6文字以上20文字以内で入力してください");
		}

		if (!loginId.matches("^[0-9a-zA-Z]+$")){
			messages.add("ログインIDは半角英数字で入力して下さい");
		}

		if ((new UserService().registerCheck(loginId, "login_id")) != null){
			messages.add("すでに使用されているログインIDです");

		}

		if (StringUtils.isBlank(password) == true || password.length() < 6 || loginId.length() > 255){
			messages.add("パスワードは6文字以上255文字以内で入力してください");
		}

		if (!password.matches("^[ -~｡-ﾟ]+$")){
			messages.add("パスワードは半角文字で入力して下さい");
		}

		if (passmatch == false){
			messages.add("パスワードが一致しません");
		}

		if (StringUtils.isBlank(name) == true) {
			messages.add("名称を入力してください");
		}

		if (name.length() > 10){
			messages.add("名称は10文字以内で入力してください");
		}

		try{
			int branch = Integer.parseInt(request.getParameter("branch"));
			int dep = Integer.parseInt(request.getParameter("dep"));
			if (branch == 0){
				messages.add("支店が選択されていません");
			}
			if (dep == 0){
				messages.add("部署・役職が選択されていません");
			}
			if (branch == 1 && (dep == 3 || dep ==4)){
				messages.add("支店と部署・役職の定義が不正です");
			}

			if (branch != 1 && (dep == 1 || dep == 2)){
				messages.add("支店と部署・役職の定義が不正です");
			}

			if (branch > 4) {
				messages.add("支店の定義が不正です");
			}

			if (dep > 4) {
				messages.add("部署・役職の定義が不正です");
			}
		}catch (NumberFormatException e){
			messages.add("支店もしくは部署・役職を半角数字で正しく入力してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
