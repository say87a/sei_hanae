package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import beans.Comment;
import beans.Log;
import beans.PostMessage;
import service.CallLogService;
import service.CommentService;
import service.MessageService;
import utils.ExplorerUtil;

@WebServlet(urlPatterns = {"/index.jsp"})
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		ArrayList<String> messages = new ArrayList<>();
		if(session.getAttribute("errorMessages") != null){
			String commentError = session.getAttribute("errorMessages").toString().replace("[","" ).replace("]", "");
			messages.add(commentError);
		}

		//TODO 検索がクソ過ぎる
		//検索条件を取得する
		String category = request.getParameter("category");
		String date1 = request.getParameter("startdate");
		String date2 = request.getParameter("enddate");

		//フォーマット変換
		String dt1 = ExplorerUtil.toDate(date1);
		String dt2 = ExplorerUtil.toDate(date2);

		if (dt1 != "error" && dt2 != "error"){
			switch (ExplorerUtil.typeDateExp(date1, date2)){
			case 1:
				//日付検索無し
				if (StringUtils.isBlank(category)){
					//全件表示
					List<PostMessage> posts = new MessageService().getMessage();
					request.setAttribute("posts", posts);
					break;
				}else{
					//カテゴリ検索
					request.setAttribute("category", category);
					List<PostMessage> posts = new MessageService().expCategoryMessage(category);
					request.setAttribute("posts", posts);
					break;
				}

			case 2:
				//日付開始日検索
				if (StringUtils.isBlank(category)){
					//日付のみ
					request.setAttribute(dt1, "startDate");
					List<PostMessage> posts = new MessageService().expDateMessageStart(dt1);
					request.setAttribute("posts", posts);
					request.setAttribute("startDate", date1);
					break;
				}else{
					//カテゴリAND
					request.setAttribute(dt1, "startDate");
					List<PostMessage> posts = new MessageService().expAllMessageStart(category, dt1);
					request.setAttribute("posts", posts);
					request.setAttribute("category", category);
					request.setAttribute("startDate", date1);
					break;
				}

			case 3:
				//日付終了日検索
				if (StringUtils.isBlank(category)){
					//日付のみ
					request.setAttribute(dt2, "endDate");
					List<PostMessage> posts = new MessageService().expDateMessageEnd(dt2);
					request.setAttribute("posts", posts);
					request.setAttribute("endDate", date2);
					break;
				}else{
					//カテゴリAND
					request.setAttribute(dt2, "endDate");
					List<PostMessage> posts = new MessageService().expAllMessageEnd(category, dt2);
					request.setAttribute("posts", posts);
					request.setAttribute("category", category);
					request.setAttribute("endDate", date2);
					break;
				}

			case 4:
				//期間順方向検索
				if (StringUtils.isBlank(category)){
					//日付のみ
					request.setAttribute("startDate", date1);
					request.setAttribute("endDate", date2);
					List<PostMessage> posts = new MessageService().expDateMessage(dt1, dt2);
					request.setAttribute("posts", posts);
					break;
				}else{
					//カテゴリAND
					request.setAttribute("category", category);
					request.setAttribute("startDate", date1);
					request.setAttribute("endDate", date2);
					List<PostMessage> posts = new MessageService().expAllMessage(category, dt1, dt2);
					request.setAttribute("posts", posts);
					break;
				}

			case 5:
				//期間逆方向検索
				if (StringUtils.isBlank(category)){
					//日付のみ
					request.setAttribute("startDate", date2);
					request.setAttribute("endDate", date1);
					List<PostMessage> posts = new MessageService().expDateMessage(dt2, dt1);
					request.setAttribute("posts", posts);
					break;
				}else{
					//カテゴリAND
					String bufferString = date1;
					date1 = date2;
					date2 = bufferString;
					new ExplorerUtil();
					request.setAttribute("category", category);
					request.setAttribute("startDate", date2);
					request.setAttribute("endDate", date1);
					List<PostMessage> posts = new MessageService().expAllMessage(category, dt2, dt1);
					request.setAttribute("posts", posts);
					break;
				}

			default:
				messages.add("日付フォーマットが不正です");
				List<PostMessage> posts = new MessageService().getMessage();
				request.setAttribute("posts", posts);

			}
		}else{
			messages.add("日付フォーマットが不正です");
			List<PostMessage> posts = new MessageService().getMessage();
			request.setAttribute("category", category);
			request.setAttribute("startDate", date1);
			request.setAttribute("endDate", date2);
			request.setAttribute("posts", posts);
		}

		List<Log> log = new CallLogService().getLog();
		request.setAttribute("log", log);


		//投稿を呼び出す
		List<Comment> comments = new CommentService().getComment();
		request.setAttribute("comments", comments);

		session.setAttribute("errorMessages", messages);

		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

	}
}