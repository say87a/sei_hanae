package controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import beans.Branch;
import beans.Dep;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.CallListService;
import service.UserService;


@WebServlet(urlPatterns = { "/HRedituser" })
@MultipartConfig(maxFileSize = 100000)
public class HREditUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		List <Branch> branchList = new CallListService().getAllBranch();
		session.setAttribute("branchList", branchList);
		List <Dep> depList = new CallListService().getAllDep();
		session.setAttribute("depList", depList);

		String editId = request.getParameter("id");
		if (!StringUtils.isBlank(editId)) {
			try {
				User editUser = new UserService().getUser(Integer.parseInt(editId));
				if (editUser != null) {
					request.setAttribute("editUser", editUser);
					request.getRequestDispatcher("/HRedituser.jsp").forward(request, response);
					return;
				}
			} catch (RuntimeException e){
				// 何もしない
			}
		}
		messages.add("不正なアクセスです");
		session.setAttribute("errorMessages", messages);
		response.sendRedirect("HRadminuser");
		//TODO 「不正なアクセス」の出るタイミングが変、自ユーザの支店役職
	}


	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();

		User editUser = getEditUser(request);
		session.setAttribute("editUser", editUser);

		if (isValid(request, messages, editUser) == true) {

			try {
				if (request.getParameter("password").isEmpty() && request.getParameter("repassword").isEmpty()){
					new UserService().updateExceptPassword(editUser);
					response.sendRedirect("HRadminuser");
					session.removeAttribute("editUser");
					return;
				}else{
					new UserService().update(editUser);
					response.sendRedirect("HRadminuser");
					session.removeAttribute("editUser");
					return;
				}
			} catch (NoRowsUpdatedRuntimeException e) {
				session.removeAttribute("editUser");
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("HRedituser");
				return;
			}

		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("HRedituser");
			return;
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLoginId(request.getParameter("loginId"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setName(request.getParameter("name"));
		try{
			editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
			editUser.setDep(Integer.parseInt(request.getParameter("dep")));
		}catch (NumberFormatException e){
			editUser.setBranch(0);
			editUser.setDep(0);
		}

		return editUser;
	}



	private boolean isValid(HttpServletRequest request, List<String> messages, User user) {
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String repassword = request.getParameter("repassword");
		boolean passmatch = password.matches(repassword);
		String name = request.getParameter("name");

			if (StringUtils.isBlank(loginId) == true || loginId.length() < 6 || loginId.length() > 20){
				messages.add("ログインIDは6文字以上20文字以内で入力してください");
			}

			if (!loginId.matches("^[0-9a-zA-Z]+$")){
				messages.add("ログインIDは半角英数字で入力して下さい");
			}
			try{
				User editUser = getEditUser(request);
				User checkUser = new UserService().registerCheck(loginId, "login_id");
				if (checkUser != null && checkUser.getId() != editUser.getId()){
					messages.add("すでに使用されているログインIDです");
				}
			}catch (ServletException e){
				messages.add("不正なアクセスです");
			}catch (IOException e){
				messages.add("不正なアクセスです");
			}

		if (!(password.isEmpty() == true && repassword.isEmpty() == true)){
			if (StringUtils.isBlank(password) == true || password.length() < 6 || loginId.length() > 255){
				messages.add("パスワードは6文字以上255文字以内で入力してください");
			}

			if (passmatch == false){
				messages.add("パスワードが一致しません");
			}


			if (!password.matches("^[ -~｡-ﾟ]+$")){
				messages.add("パスワードは半角文字で入力して下さい");
			}
		}

		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		}

		if (name.length() > 10){
			messages.add("名前は10文字以内で入力してください");
		}

		try{
			int branch = Integer.parseInt(request.getParameter("branch"));
			int dep = Integer.parseInt(request.getParameter("dep"));
			if (branch == 0){
				messages.add("支店が選択されていません");
			}
			if (dep == 0){
				messages.add("部署・役職が選択されていません");
			}
			if (branch == 1 && (dep == 3 || dep ==4)){
				messages.add("支店と部署・役職の定義が不正です");
			}

			if (branch != 1 && (dep == 1 || dep == 2)){
				messages.add("支店と部署・役職の定義が不正です");
			}

			if (branch > 4) {
				messages.add("支店の定義が不正です");
			}

			if (dep > 4) {
				messages.add("部署・役職の定義が不正です");
			}

			if (user.getId() == ((User)request.getSession().getAttribute("loginUser")).getId() && ((branch != ((User)request.getSession().getAttribute("loginUser")).getBranch() || dep != ((User)request.getSession().getAttribute("loginUser")).getBranch()))){
				messages.add("自身の支店と部署・役職は変更出来ません");
			}


		}catch (NumberFormatException e){
			messages.add("支店もしくは部署・役職を半角数字で正しく入力してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
