package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Log;
import dao.PostlogDao;

public class CallLogService {
	public void insertPost(Log log) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostlogDao postlogDao = new PostlogDao();
			postlogDao.insertPost(connection, log);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void insertComment(Log log) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostlogDao postlogDao = new PostlogDao();
			postlogDao.insertComment(connection, log);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}


	private static final int LIMIT_NUM = 5;

	public List<Log> getLog() {

		Connection connection = null;
		try {
			connection = getConnection();

			PostlogDao postlogDao = new PostlogDao();
			List<Log> ret = postlogDao.getLog(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}



}
