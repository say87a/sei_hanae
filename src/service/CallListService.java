package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Branch;
import beans.Dep;
import dao.PulldownDao;

public class CallListService {
	public List<Branch> getAllBranch() {

		Connection connection = null;
		try {
			connection = getConnection();

			PulldownDao pulldownDao = new PulldownDao();
			List<Branch> ret = pulldownDao.getAllBranch(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<Dep> getAllDep() {

		Connection connection = null;
		try {
			connection = getConnection();

			PulldownDao pulldownDao = new PulldownDao();
			List<Dep> ret = pulldownDao.getAllDep(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
