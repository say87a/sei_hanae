package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.PostMessage;
import dao.MessageDao;
import dao.PostMessageDao;

public class MessageService {

	public void register(PostMessage message) {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.insert(connection, message);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}


	public void update(PostMessage message) {

		Connection connection = null;
		try {
			connection = getConnection();

			new PostMessageDao();
			PostMessageDao.update(connection, message);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;

	public List<PostMessage> getMessage() {

		Connection connection = null;
		try {
			connection = getConnection();

			PostMessageDao messageDao = new PostMessageDao();
			List<PostMessage> ret = messageDao.getPostMessages(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//日付検索用
	public List<PostMessage> expDateMessage(String dt1, String dt2) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostMessageDao messageDao = new PostMessageDao();
			List<PostMessage> ret = messageDao.expPostMessages(connection, dt1, dt2, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<PostMessage> expDateMessageStart(String dt1) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostMessageDao messageDao = new PostMessageDao();
			List<PostMessage> ret = messageDao.expPostMessagesStart(connection, dt1, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<PostMessage> expDateMessageEnd(String dt2) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostMessageDao messageDao = new PostMessageDao();
			List<PostMessage> ret = messageDao.expPostMessagesEnd(connection, dt2, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<PostMessage> expAllMessage(String cate, String dt1, String dt2) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostMessageDao messageDao = new PostMessageDao();
			List<PostMessage> ret = messageDao.expPostMessages(connection, cate, dt1, dt2, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<PostMessage> expAllMessageStart(String cate, String dt1) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostMessageDao messageDao = new PostMessageDao();
			List<PostMessage> ret = messageDao.expPostMessagesStart(connection, cate, dt1, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public List<PostMessage> expAllMessageEnd(String cate, String dt2) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostMessageDao messageDao = new PostMessageDao();
			List<PostMessage> ret = messageDao.expPostMessagesEnd(connection, cate, dt2, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//カテゴリ検索用
	public List<PostMessage> expCategoryMessage(String cate) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostMessageDao messageDao = new PostMessageDao();
			List<PostMessage> ret = messageDao.expPostMessages(connection, cate, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public PostMessage getMessage(Integer userId) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostMessageDao messageDao = new PostMessageDao();
			PostMessage ret = messageDao.getPostMessages(connection, userId, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public PostMessage getMaxPostId() {

		Connection connection = null;
		try {
			connection = getConnection();

			PostMessageDao postMessageDao = new PostMessageDao();
			PostMessage postMessage = postMessageDao.getMaxPostId(connection);

			commit(connection);

			return postMessage;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}


}
