package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")

public class ForbiddenFilter extends HttpServlet implements Filter {

	@Override
	public void init(FilterConfig objFc) throws ServletException {

		System.out.println("ユーザー認証を行います。");
	}

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		User user;
		ArrayList<String> errorMessages = new ArrayList<>();
		HttpSession session = ((HttpServletRequest) request).getSession();

		user = (User) session.getAttribute("loginUser");
		String loginRegex = "^/log";
		String styleSheet = ".css$";
		String adminRegex = "^/HR";
		Pattern loginP = Pattern.compile(loginRegex);
		Pattern styleP = Pattern.compile(styleSheet);
		Pattern adminP = Pattern.compile(adminRegex);

		String servletPath = ((HttpServletRequest) request).getServletPath();

		if (!loginP.matcher(servletPath).find()
				&& !styleP.matcher(servletPath).find()) {
			if (!styleP.matcher(servletPath).find() && user == null) {
				errorMessages.add("ログインされていません");
				session.setAttribute("errorMessages", errorMessages);
				((HttpServletResponse) response).sendRedirect("login");
				return;
			} else {

				if (!styleP.matcher(servletPath).find()
						&& adminP.matcher(servletPath).find() == true) {
					if (user.getDep() != 1) {
						errorMessages.add("アクセス権限がありません");
						session.setAttribute("errorMessages", errorMessages);
						((HttpServletResponse) response).sendRedirect("./");
						return;
					}
					chain.doFilter(request, response);
					return;
				}
				chain.doFilter(request, response);
				return;
			}
		}
		chain.doFilter(request, response);
		return;
	}
}
