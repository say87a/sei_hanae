<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ユーザー編集</title>
</head>
<body>
<div id="wrap">
	<div id="header">
	<!-- c:if ${not empty loginUser}省略 -->
		<h1 align="center">わったい菜コミュニティ</h1>

			<div align="right">こんにちは、<c:out value="${loginUser.name}" />さん</div><br/>

	</div>

	<div id = menu>
	<ul id="menu">
		<li class="first active"><a href="./">ホーム</a></li>
		<li><a href="newpost.jsp">新規投稿</a></li>
		<li><a href = "HRadminuser">ユーザー管理</a></li>
		<li><a href=""></a></li>
		<li class="last active"><a href="logout">ログアウト</a></li>
	</ul>
	<br/>
	</div>
<div id="contents">
<h3>ユーザー編集</h3>


<c:if test="${ not empty errorMessages }">
	<div class="errorMessage">
		<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>

<div class="sel">
<form action="HRedituser" method="post"><br />
	<table id="form">
	<tr><td><label for="loginId"><b>ログインID</b> </label></td>
	<td><input name="loginId" value="${editUser.loginId}" id="loginId"/> ※半角英数字6～20文字</td></tr>

	<tr><td><label for="password"><b>パスワード</b> </label></td>
	<td><input name="password" type="password" id="password"/> ※半角文字6～255文字 </td></tr>

	<tr><td><label for="repassword"><b>パスワード(確認用)</b> </label></td>
	<td><input name="repassword" type="password" id="repassword" /></td></tr>

	<tr><td><label for="name"><b>名称 </b></label></td>
	<td><input name="name" value="${editUser.name}" /> ※10文字以内</td></tr>

	<tr>
		<td>
			<label for="branch"><b>支店 </b></label>
		</td>
		<td>
			<c:if test="${loginUser.id == editUser.id}">
				<c:forEach items="${branchList}" var="branch">
						<c:if test="${branch.id == editUser.branch}" var="selectBranch"/>
						<c:if test="${selectBranch}">
							<c:out value="${branch.branchName}" />
							<input type="hidden" name="branch" value="${branch.id}"/>
						</c:if>
				</c:forEach>
			</c:if>
			<c:if test="${loginUser.id != editUser.id}">
				<select name="branch">
					<option value="0"> </option>
					<c:forEach items="${branchList}" var="branch">
						<c:if test="${branch.id == editUser.branch}" var="selectBranch"/>
						<c:if test="${selectBranch}"><option value="${branch.id}" selected><c:out value="${branch.branchName}"/></option></c:if>
						<c:if test="${!selectBranch}"><option value="${branch.id}"><c:out value="${branch.branchName}"/></option></c:if>
					</c:forEach>
				</select>
			</c:if>
		</td>
	</tr>

	<tr>
		<td>
			<label for="dep"><b>部署・役職</b> </label>
		</td>
		<td>
			<c:if test="${loginUser.id == editUser.id}">
				<c:forEach items="${depList}" var="dep">
						<c:if test="${dep.id == editUser.dep}" var="selectDep"/>
						<c:if test="${selectDep}">
							<c:out value="${dep.depName}" />
							<input type="hidden" name="dep" value="${dep.id}"/>
						</c:if>
				</c:forEach>
			</c:if>
			<c:if test="${loginUser.id != editUser.id}">
				<select name="dep">
					<option value="0"> </option>
					<c:forEach items="${depList}" var="dep">
						<c:if test="${dep.id == editUser.dep}" var="selectDep"/>
						<c:if test="${selectDep}"><option value="${dep.id}" selected><c:out value="${dep.depName}"/></option></c:if>
						<c:if test="${!selectDep}"><option value="${dep.id}"><c:out value="${dep.depName}"/></option></c:if>
					</c:forEach>
				</select>
			</c:if>
		</td>
	</tr>

	<tr>
		<td></td>
		<td>
			<button type="submit">登録</button>
		</td>
	</tr>
</table>
<input type="hidden" name="id" value="${editUser.id}" />
</form>
</div>
</div>
<div id="footer"><hr/>Copyright (c) 2015 Hanae Sei All Rights Reserved.</div>
</div>
</body>
</html>