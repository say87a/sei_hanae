<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
<!--
	function stop(){
		if(window.confirm('このユーザーを停止しますか？')){
			return true;		//確定後の動作
		}else{
			return false;
		}
	}

	function restart(){
		if(window.confirm('このユーザーを復活しますか？')){
			return true;		//確定後の動作
		}else{
			return false;
		}

	function alert(){
		window.alert("自身のアカウントは操作できません");
	}

}
-->
</script>
</head>
<body>
<div id="wrap">
	<div id="header">
	<!-- c:if ${not empty loginUser}省略 -->
		<h1 align="center">わったい菜コミュニティ</h1>

			<div align="right">こんにちは、<c:out value="${loginUser.name}" />さん</div><br/>

	</div>
	<div id = menu>
	<ul id="menu">
		<li class="first active"><a href="./">ホーム</a></li>
		<li><a href="newpost.jsp">新規投稿</a></li>
		<li><a href = "HRadminuser">ユーザー管理</a></li>
		<li><a href="HRsignup">ユーザー新規登録</a></li>
		<li class="last active"><a href="logout">ログアウト</a></li>
	</ul>
	<br/>
	</div>
<div id="contents">
<!-- コメント用エラーメッセージ -->
<c:if test="${not empty errorMessages }">
	<div class = "errorMessage">
		<ul>
			<c:forEach items = "${errorMessages}" var = "message">
			<li><c:out value="${message}"/>
			</c:forEach>
		</ul>
	</div>
	<c:remove var = "errorMessages" scope = "session"/>
</c:if>


<h3>ユーザー管理</h3>

<table id="user">
	<tr><th>ログインID</th><th>名称</th><th>支店</th><th>部署・役職</th><th>状態</th><th> </th></tr>

	<c:forEach items = "${users}" var="user">
		<tr><td><c:out value="${user.loginId}"/></td><td><c:out value="${user.name}"/></td><td><c:out value="${user.branchName}"/></td>
		<td><c:out value="${user.depName}"/></td><td>

		<c:if test="${user.id == loginUser.id }" var="self"/>
		<c:if test="${user.status == 'true'}" var="flg"/>
		<c:if test="${flg}" >使用中<br/>
			<div class="button">
				<form method="POST" action="HRstopUser?id=${user.id}" onSubmit="return stop()">
					<div align="center">
						<button type="submit" ${self ? 'disabled' : ''}>停止する</button>
					</div>
				</form>
			</div>
		</c:if>
		<c:if test="${!flg}"><div class="error">停止中<br/></div>
			<div class="button">
				<form method="POST" action="HRrestartUser?id=${user.id}" onSubmit="return restart()">
					<div align="center">
						<button type="submit">復活する</button>
					</div>
				</form>
			</div>
		</c:if>



		</td><td><a href="HRedituser?id=${user.id}">編集</a></td></tr>
	</c:forEach>
</table>
</div>
<div id="footer"><hr/>Copyright (c) 2015 Hanae Sei All Rights Reserved.</div>
</div>
</body>
</html>