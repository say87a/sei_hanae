<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>新規投稿</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="wrap">

	<div id="header">
	<!-- c:if ${not empty loginUser}省略 -->
		<h1 align="center">わったい菜コミュニティ</h1>

			<div align="right">こんにちは、<c:out value="${loginUser.name}" />さん</div><br/>

	</div>

	<div id = menu>
	<ul id="menu">
		<li class="first active"><a href="./">ホーム</a></li>
		<li><a href="newpost.jsp">新規投稿</a></li>

		<c:if test="${loginUser.dep == 1 }" var="dep"/>
		<c:if test="${dep}">
			<li><a href = "HRadminuser">ユーザー管理</a></li>
		</c:if>
		<c:if test="${!dep }">
			<li><a href=""></a></li>
		</c:if>
		<li><a href=""></a></li>
		<li class="last active"><a href="logout">ログアウト</a></li>
	</ul>
	<br/>
	</div>
<div id="contents">
<h3>新規投稿</h3>
<div class="errorMessage">
	<c:if test="${not empty errorMessages }">
		<div class = "errorMessage">
			<ul>
				<c:forEach items = "${errorMessages}" var = "message">
				<li><c:out value="${message}"/>
				</c:forEach>
			</ul>
		</div>
		<c:remove var = "errorMessages" scope = "session"/>
	</c:if>

</div>

	<div class="sel">
		<form id="signup" action="newpost" method="post">
		<table id="post">
		<tr><td><label for="title"><span><b>タイトル </b> </span></label></td>
		<td><input name="title" type="text" id="title" value="${input.title}"> ※50文字まで</td></tr>
		<tr><td><label for="category"><span><b>カテゴリ </b> </span></label></td>
		<td><input name="category" type="text" id="category" value="${input.category}"> ※10文字まで</td></tr>
		<tr><td><label for="text"><span><b>本文 </b></span></label></td>
		<td><textarea name="text" cols="100" rows="5" id="text"><c:out value="${input.text}"/></textarea> ※1000文字まで</td></tr>
		<tr><td><label for="button"><span> </span></label></td>
		<td><button  name="submit" type="submit">投稿する</button></td></tr>
		</table>


		</form>
	</div>

</div>
<div id="footer"><hr/>Copyright (c) 2015 Hanae Sei All Rights Reserved.</div>

</div>
</body>
</html>
