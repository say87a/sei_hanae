<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>わったい菜コミュニティ</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<script type="text/javascript"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
	<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/south-street/jquery-ui.css" >

	<script>
		  $(function() {
		    $("#startdate,#enddate").datepicker();
		  });
	</script>
</head>
<body>
<div id="wrap">


	<div id="header">
	<!-- c:if ${not empty loginUser}省略 -->
		<h1 align="center">わったい菜コミュニティ</h1>

			<div align="right">こんにちは、<c:out value="${loginUser.name}" />さん</div><br/>

	</div>

	<div id = menu>
	<ul id="menu">
		<li class="first active"><a href="./">ホーム</a></li>
		<li><a href="newpost">新規投稿</a></li>

		<c:if test="${loginUser.dep == 1 }" var="dep"/>
		<c:if test="${dep}">
			<li><a href = "HRadminuser">ユーザー管理</a></li>
		</c:if>
		<c:if test="${!dep }">
			<li><a href=""></a></li>
		</c:if>
		<li><a href=""></a></li>
		<li class="last active"><a href="logout">ログアウト</a></li>
	</ul>
	<br/>
	</div>
	<br/>

<div id="contents">

	<!-- コメント用エラーメッセージ -->
	<div class="errorMessage">
	<c:if test="${not empty errorMessages }">
			<ul>
				<c:forEach items = "${errorMessages}" var = "message">
				<li><c:out value="${message}"/>
				</c:forEach>
			</ul>
		<c:remove var = "errorMessages" scope = "session"/>
	</c:if>
	</div>

<%-- 表示できなかったら削除--%>
	<div class="update-title">更新履歴</div>
	<div class="update">
	<ul>
	<c:forEach items="${log}" var="list">
		<c:if test="${list.styleType == 0 }" var="style"/>
		<c:if test="${style}">
			<li><a href="#${list.postId }"> <fmt:formatDate value="${list.updateDate}" pattern="yyyy/MM/dd" /> <c:out value="${list.name}"/>さんが「<c:out value="${list.title}"/>」を投稿しました</a></li>
		</c:if>
		<c:if test="${!style}">
			<li><a href="#${list.postId}"> <fmt:formatDate value="${list.updateDate}" pattern="yyyy/MM/dd" /> <c:out value="${list.name}"/>さんが「<c:out value="${list.title}"/>」にコメントしました</a></li>
		</c:if>
	</c:forEach>
	</ul>
	</div>

	<!-- 検索用窓 -->
	<div class="explorer">
	<form action="./" method="get">
	<fieldset>
		<legend>投稿を検索</legend>
	<div class="sel">
	<table id="explorer">
		<tr><td><label for="category"><b>カテゴリを入力</b></label></td>
		<td><input name="category" type="text" value="${category}"></td><td rowspan="2"><button  type="submit">検索する</button></td></tr>
		<tr><td><b>検索期間を入力</b> (yyyy/MM/ddで入力 ex.2015/10/01)</td>
		<td><input name="startdate" type="text" id="startdate" value="${startDate}">
		<label for="startdate">から</label>
		<input name="enddate" type="text" id="enddate" value="${endDate}">
		<label for="enddate">まで</label><br/></td></tr>

	</table>
	</div>
	</fieldset>
	</form>
	</div>
	<hr/>
	<!-- 投稿表示 -->

	<div class="message">
	<c:if test="${empty posts }">
		<div class="error">該当する投稿がありません</div>
	</c:if>
	<c:if test="${not empty posts }">
		<c:forEach items="${posts}" var="post" varStatus="pstat">
					<div class="message-header" id="${post.id}">
					<div class="message-title" style="float:left"><c:out value="${post.title}" /></div>  <div class="message-category" style="float:right">カテゴリ: <c:out value="${post.category}" /></div>
					</div>

					<div class="message-text"><pre><c:out value="${post.text}" /></pre></div>

					<div class="message-footer">
						<span class="name">投稿者: <c:out value="${post.branchName}" /> <c:out value="${post.depName}" /> <c:out value="${post.userName}" /></span>
						<fmt:formatDate value="${post.insertDate}" pattern="yyyy/MM/dd HH:mm:ss" />
						<c:if test="${post.userId == loginUser.id || loginUser.dep == 2 || (loginUser.dep == 3 && post.branchId == loginUser.branch)}">
						<a href="javascript:postDelete${pstat.index}.submit();" onClick="return confirm('本当に削除しますか？')">削除</a></c:if>
						<form name="postDelete${pstat.index}" action="delete" method="post">
							<input type="hidden" name="pdelId" value="${post.id}" />
						</form>
					</div>



				<c:forEach items="${comments}" var="comment" varStatus="cstat">

					<c:if test="${comment.postId==post.id}">
					<div class="comment">
						<div class="message-comment"><pre><c:out value="${comment.text}" /></pre></div>

						<div class="message-footer">
						<span class="name">投稿者: <c:out value="${comment.branchName}" /> <c:out value="${comment.depName}" /> <c:out value="${comment.userName}" /></span>
						<fmt:formatDate value="${comment.postDate}" pattern="yyyy/MM/dd HH:mm:ss" />
						<c:if test="${comment.userId == loginUser.id || loginUser.dep == 2 || (loginUser.dep == 3 && comment.branchId == loginUser.branch)}">
							<a href="javascript:commentDelete${cstat.index}.submit();" onClick="return confirm('本当に削除しますか？');">削除</a>
							<form name="commentDelete${cstat.index}" action="delete" method="post">
								<input type="hidden" name="cdelId" value="${comment.id}" />
							</form>
						</c:if>
						</div>
					</div>
					</c:if>

				</c:forEach>




			<div class="newComment">
			<form action="newcomment?postId=${post.id}" method="post">
				<label for="reply"><b>この投稿に返信</b> ※500文字以内</label><br/>
				<textarea name="comment"  cols="100" rows="5" class="tweet-box"></textarea><br/>
				<button type="submit">コメントする</button><br/>
			</form>
			</div>

		</c:forEach>
	</c:if>
	</div>
</div>




<div id="footer"><hr/>Copyright (c) 2015 Hanae Sei All Rights Reserved.</div>
</div>
</body>
</html>
