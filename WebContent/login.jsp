<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored = "false" %>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>	ログイン</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="wrap">
<div id= "contents">
<div class = "errorMessage">
<c:if test="${not empty errorMessages }">

		<ul>
			<c:forEach items = "${errorMessages}" var = "message">
			<li><c:out value="${message}"/>
			</c:forEach>
		</ul>

	<c:remove var = "errorMessages" scope = "session"/>
</c:if>
</div>

<form action = "login" method = "post"><br/>
<table id="form">
	<tr><td><label for = "loginId">ログインID</label></td>
	<td><input name = "loginId" id = "loginId"/> </td></tr>

	<tr><td><label for = "password">パスワード </label></td>
	<td><input name = "password" type = "password" id = "password"/> </td></tr>

	<tr><td></td><td><button type = "submit">ログイン</button> </td></tr>
</table>
</form>
</div>

<div id="footer"><hr/>Copyright (c) 2015 Hanae Sei All Rights Reserved.</div>

</div>
</body>
</html>